package jp.co.lerning.service;

import java.util.LinkedList;
import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import jp.co.lerning.dto.LerningDto;
import jp.co.lerning.entity.LerningEntity;
import jp.co.lerning.mapper.LerningMapper;

@Service
public class LerningService {
	@Autowired
	private LerningMapper lerningMapper;

	//全件取得
	public List<LerningDto> getAllId() {
		List<LerningEntity> idList = lerningMapper.getAllId();
		List<LerningDto> resultList = convertToDto(idList);
		return resultList;
	}

	private List<LerningDto> convertToDto(List<LerningEntity> idList) {//ここのidList
		List<LerningDto> resultList = new LinkedList<>();
		for (LerningEntity entity : idList) {//とここのtextListが対応
			LerningDto dto = new LerningDto();
			BeanUtils.copyProperties(entity, dto);
			resultList.add(dto);
		}
		return resultList;
	}
}
