package jp.co.lerning.service;

import java.util.LinkedList;
import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import jp.co.lerning.dto.ExplanationDto;
import jp.co.lerning.entity.ExplanationEntity;
import jp.co.lerning.mapper.ExplanationMapper;

@Service
public class ExplanationService {
	@Autowired
    private ExplanationMapper explanationMapper;

	//全件取得
    public List<ExplanationDto> getExplanation() {
        List<ExplanationEntity> explanationList = explanationMapper.getExplanation();
        List<ExplanationDto> resultList = convertToDto(explanationList);
        return resultList;
    }

    private List<ExplanationDto> convertToDto(List<ExplanationEntity> explanationList) {//ここのtextList
        List<ExplanationDto> resultList = new LinkedList<>();
        for (ExplanationEntity entity : explanationList) {//とここのtextListが対応
            ExplanationDto dto = new ExplanationDto();
            BeanUtils.copyProperties(entity, dto);
            resultList.add(dto);
        }
        return resultList;
    }
}
