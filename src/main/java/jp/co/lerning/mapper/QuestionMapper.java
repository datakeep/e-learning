package jp.co.lerning.mapper;

import java.util.List;

import jp.co.lerning.entity.QuestionEntity;

public interface QuestionMapper {
	List<QuestionEntity> getQuestionAll();
}
