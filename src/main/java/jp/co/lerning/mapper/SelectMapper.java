package jp.co.lerning.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import jp.co.lerning.entity.SelectEntity;

public interface SelectMapper {

	//全件取得
	List<SelectEntity> getSelectAll();

	//データベースにユーザーが選んだ番号のid追加
	int insertSelect(@Param("choice_id")int choiceId, @Param("question_id")int questionId);
}