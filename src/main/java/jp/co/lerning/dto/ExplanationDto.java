package jp.co.lerning.dto;

public class ExplanationDto {
    private String text;
    private Integer questionId;

	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	}
	public Integer getQuestionId() {
		return questionId;
	}
	public void setQuestionId(Integer questionId) {
		this.questionId = questionId;
	}
}
