<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>結果＆採点画面</title>
</head>

<body>
	<!-- ②採点結果の表示 -->
	<c:forEach items="${select}" var="select">
		<c:forEach items="${ids}" var="answers">
				<c:if test="${answers.answerId == select.choiceId  }">
					<div class="answer">
						<c:out value="100点中${result}点です" />
					</div>
				</c:if>
		</c:forEach>
	</c:forEach>

	<!-- 正解 -->
	<c:forEach items="${select}" var="user">
		<c:forEach items="${ids}" var="answers">

			<c:if test="${answers.questionId == user.questionId}">
				<!-- ユーザーがjspで入力した問題と解答のIdどう書く？？-->
				<c:if test="${answers.answerId == user.choiceId}">
					<div class="answer">
						<c:out value="${good}" />
					</div>
				</c:if>
			</c:if>

			<!-- 不正解 -->
			<c:if test="${answers.questionId == user.questionId}">
				<c:if test="${answers.answerId != user.choiceId}">
					<div class="answer">
						<c:out value="${bad}" />
					</div>
				</c:if>
			</c:if>
			<div class="questions">

				<form:form modelAttribute="selectInsert">
					<c:forEach items="${questions}" var="question">
						<div class="question">
							<span class="text"><c:out value="${question.id}" /></span>
							<c:out value="${question.text}" />
							<br />
						</div>
						<div class="selects">
							<c:if test="${question.id == select.questionId }">
								<c:forEach items="${selects}" var="select">
									<form:form modelAttribute="SelectForm">
										<span class="select1"> <c:out value="${select.text}" /><input id="tel1" name="select1" type="radio" value="select" /></span>
									<br />
									</form:form>
								</c:forEach>
							</c:if>
						</div>
					</c:forEach>
				</form:form>
			</div>
		</c:forEach>
		<h1>${sentence}</h1>
		<h2>${texts.text}</h2>
	</c:forEach>
</body>
</html>